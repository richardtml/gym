package omx.org.leetcode.medium.problem494;

import static org.junit.Assert.assertEquals;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.junit.Test;

/**

494. Target Sum

You are given a list of non-negative integers, a1, a2, ..., an, and a target, S. Now you have 2 symbols + and -. For each integer, you should choose one from + and - as its new symbol.

Find out how many ways to assign symbols to make sum of integers equal to target S.

Example 1:

Input: nums is [1, 1, 1, 1, 1], S is 3.
Output: 5
Explanation:

-1+1+1+1+1 = 3
+1-1+1+1+1 = 3
+1+1-1+1+1 = 3
+1+1+1-1+1 = 3
+1+1+1+1-1 = 3

There are 5 ways to assign symbols to make the sum of nums be target 3.

Note:

    The length of the given array is positive and will not exceed 20.
    The sum of elements in the given array will not exceed 1000.
    Your output answer is guaranteed to be fitted in a 32-bit integer.

 */

public class Solution {

	// fine but slow :/
	public int findTargetSumWays1(int[] nums, int S) {
		return findTargetSumWays1(nums, S, 0, 0);
	}

	private int findTargetSumWays1(int[] nums, int S, int acc, int i) {
		if (acc == S && nums.length == i) return 1;
		if (nums.length == i) return 0;
		return 	findTargetSumWays1(nums, S, acc + nums[i], i + 1) +
				findTargetSumWays1(nums, S, acc - nums[i], i + 1);
	}

	@Test
	public void testfindTargetSumWays1() {
		int[] nums = {1, 1, 1, 1, 1};
		int S = 3;
		int expected = 5;
		Solution sol = new Solution();
		int actual = sol.findTargetSumWays1(nums, S);
		assertEquals(expected, actual);
	}

}

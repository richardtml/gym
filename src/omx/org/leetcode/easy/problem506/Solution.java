package omx.org.leetcode.easy.problem506;

import static org.junit.Assert.assertArrayEquals;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Test;

/**

506. Relative Ranks

Given scores of N athletes, find their relative ranks and the people with the top three highest scores, who will be awarded medals: "Gold Medal", "Silver Medal" and "Bronze Medal".

Example 1:

Input: [5, 4, 3, 2, 1]
Output: ["Gold Medal", "Silver Medal", "Bronze Medal", "4", "5"]
Explanation: The first three athletes got the top three highest scores, so they got "Gold Medal", "Silver Medal" and "Bronze Medal".
For the left two athletes, you just need to output their relative ranks according to their scores.

Note:

    N is a positive integer and won't exceed 10,000.
    All the scores of athletes are guaranteed to be unique.

 */

public class Solution {

	public String[] findRelativeRanks(int[] nums) {
		String[] ranks = new String[nums.length];

		Rank[] tmp = new Rank[nums.length];
		for (int i = 0; i < nums.length; i++)
			tmp[i] = new Rank(nums[i], i);
		Arrays.sort(tmp, Collections.reverseOrder());

		for (int i = 0; i < tmp.length; i++) {
			String rank;
			if (i == 0)			rank = "Gold Medal";
			else if (i == 1)	rank = "Silver Medal";
			else if (i == 2)	rank = "Bronze Medal";
			else				rank = Integer.toString(i + 1);
			ranks[tmp[i].index] = rank;
		}
		return ranks;
	}

	static class Rank implements Comparable<Rank> {
		public int score;
		public int index;

		public Rank(int score, int index) {
			this.score = score;
			this.index = index;
		}

		@Override
		public int compareTo(Rank that) {
			return this.score - that.score;
		}
	}

	@Test
	public void testFindRelativeRanks() {
		int[] nums = {5, 4, 3, 2, 1};
		String[] expected = {"Gold Medal", "Silver Medal", "Bronze Medal", "4", "5"};
		Solution sol = new Solution();
		String[] actual = sol.findRelativeRanks(nums);
		assertArrayEquals(expected, actual);
	}

}

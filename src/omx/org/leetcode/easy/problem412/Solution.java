package omx.org.leetcode.easy.problem412;

import static org.junit.Assert.assertArrayEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

/**

412. Fizz Buzz

Write a program that outputs the string representation of numbers from 1 to n.

But for multiples of three it should output “Fizz” instead of the number and for the multiples of five output “Buzz”. For numbers which are multiples of both three and five output “FizzBuzz”.

Example:

n = 15,

Return:
[
    "1",
    "2",
    "Fizz",
    "4",
    "Buzz",
    "Fizz",
    "7",
    "8",
    "Fizz",
    "Buzz",
    "11",
    "Fizz",
    "13",
    "14",
    "FizzBuzz"
]

 */

public class Solution {

    public List<String> fizzBuzz(int n) {
    	List<String> fb = new ArrayList<>();
    	for (int i = 1; i <= n; i++) {
			boolean mul3 = i % 3 == 0;
			boolean mul5 = i % 5 == 0;
			if (mul3 && !mul5) {
				fb.add("Fizz");
			} else if (!mul3 && mul5) {
				fb.add("Buzz");
			} else if (mul3 && mul5) {
				fb.add("FizzBuzz");
			} else {
				fb.add(Integer.toString(i));
			}
		}
    	return fb;
    }

	@Test
	public void test() {
		int input = 15;
		String[] ex = {
		    "1",
		    "2",
		    "Fizz",
		    "4",
		    "Buzz",
		    "Fizz",
		    "7",
		    "8",
		    "Fizz",
		    "Buzz",
		    "11",
		    "Fizz",
		    "13",
		    "14",
		    "FizzBuzz"
		};
		List<String> expected = Arrays.asList(ex);
		Solution sol = new Solution();
		List<String> actual = sol.fizzBuzz(input);
		assertArrayEquals(expected.toArray(), actual.toArray());
	}

}

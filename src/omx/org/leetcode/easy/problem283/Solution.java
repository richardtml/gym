package omx.org.leetcode.easy.problem283;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

/**

283. Move Zeroes

Given an array nums, write a function to move all 0's to the end of it while maintaining the relative order of the non-zero elements.

For example, given nums = [0, 1, 0, 3, 12], after calling your function, nums should be [1, 3, 12, 0, 0].

Note:

    You must do this in-place without making a copy of the array.
    Minimize the total number of operations.

Credits:
Special thanks to @jianchao.li.fighter for adding this problem and creating all test cases.

 */

public class Solution {

	public void moveZeroes(int[] nums) {
		int top = 0;
		for (int i = 0; i < nums.length; i++) {
			if (nums[i] != 0) {
				if (top != i) {
					nums[top] = nums[i];
				}
				top++;
			}
		}
		for (int i = top; i < nums.length; i++) {
			nums[i] = 0;
		}
	}

	@Test
	public void testMoveZeroes() {
		int[] nums = {0, 1, 0, 3, 12};
		int[] expected = {1, 3, 12, 0, 0};
		Solution sol = new Solution();
		sol.moveZeroes(nums);
		assertArrayEquals(expected, nums);
	}

}

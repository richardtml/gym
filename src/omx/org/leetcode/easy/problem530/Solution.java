package omx.org.leetcode.easy.problem530;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

/**

530. Minimum Absolute Difference in BST

Given a binary search tree with non-negative values, find the minimum absolute difference between values of any two nodes.

Example:

Input:

   1
    \
     3
    /
   2

Output:
1

Explanation:
The minimum absolute difference is 1, which is the difference between 2 and 1 (or between 2 and 3).

Note: There are at least two nodes in this BST.

 */

public class Solution {

    public int getMinimumDifference(TreeNode root) {
    	List<Integer> list = new ArrayList<>();
    	inorder(root, list);
    	int min = list.get(list.size() - 1) - list.get(0);
    	for (int i = 0; i < list.size() - 1; i++) {
			int dif =  list.get(i + 1) - list.get(i);
			if (dif < min) {
				min = dif;
			}
		}

    	return min;
    }

    private void inorder(TreeNode root, List<Integer> list) {
    	if (root == null) return;
    	inorder(root.left, list);
    	list.add(root.val);
    	inorder(root.right, list);
    }


	public static class TreeNode {
		public int val;
		public TreeNode left;
		public TreeNode right;
		public TreeNode(int x) { val = x; }
	}

	@Test
	public void test() {
		TreeNode c = new TreeNode(2);
		TreeNode b = new TreeNode(3);
		b.left = c;
		TreeNode a = new TreeNode(1);
		a.right = b;
		int expected = 1;

		Solution sol = new Solution();
		int actual = sol.getMinimumDifference(a);

		assertEquals(expected, actual);
	}

}

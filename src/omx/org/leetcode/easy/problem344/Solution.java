package omx.org.leetcode.easy.problem344;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**

344. Reverse String

Write a function that takes a string as input and returns the string reversed.

Example:
Given s = "hello", return "olleh".

 */

public class Solution {

	public String reverseString(String s) {
		StringBuilder sb = new StringBuilder(s.length());
		for (int i = s.length() - 1; i >= 0; i--) {
			sb.append(s.charAt(i));
		}
		return sb.toString();
	}

	@Test
	public void testReverseString() {
		String input = "hello";
		String expected = "olleh";
		Solution sol = new Solution();
		String actual = sol.reverseString(input);
		assertEquals(expected, actual);
	}

}

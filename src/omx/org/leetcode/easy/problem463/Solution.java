package omx.org.leetcode.easy.problem463;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**

 463. Island Perimeter
DescriptionHintsSubmissionsSolutions

    Total Accepted: 35649 Total Submissions: 62794 Difficulty: Easy
    Contributors: amankaraj

You are given a map in form of a two-dimensional integer grid where 1 represents land and 0 represents water. Grid cells are connected horizontally/vertically (not diagonally). The grid is completely surrounded by water, and there is exactly one island (i.e., one or more connected land cells). The island doesn't have "lakes" (water inside that isn't connected to the water around the island). One cell is a square with side length 1. The grid is rectangular, width and height don't exceed 100. Determine the perimeter of the island.

Example:

[[0,1,0,0],
 [1,1,1,0],
 [0,1,0,0],
 [1,1,0,0]]

Answer: 16
Explanation: The perimeter is the 16 yellow stripes in the image below:

 */

public class Solution {

	public int islandPerimeter(int[][] grid) {
		int perimeter = 0;
		for (int i = 0; i < grid.length; i++) {
			for (int j = 0; j < grid[0].length; j++) {
				if (isLand(i, j, grid)) {
					if (!isLand(i + 1, j, grid))
						perimeter++;
					if (!isLand(i - 1, j, grid))
						perimeter++;
					if (!isLand(i, j + 1, grid))
						perimeter++;
					if (!isLand(i, j - 1, grid))
						perimeter++;
				}
			}
		}
		return perimeter;
	}

	private boolean isLand(int i, int j, int[][] grid) {
		if (i < 0 || i >= grid.length || j < 0 || j >= grid[0].length)
			return false;
		if (grid[i][j] == 0)
			return false;
		return true;
	}

	@Test
	public void testIslandPerimeter() {
		int[][] grid = {
			{0,1,0,0},
			{1,1,1,0},
			{0,1,0,0},
			{1,1,0,0}
		};
		int expected = 16;
		Solution sol = new Solution();
		int actual = sol.islandPerimeter(grid);
		assertEquals(expected, actual);
	}

}

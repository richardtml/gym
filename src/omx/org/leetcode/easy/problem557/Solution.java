package omx.org.leetcode.easy.problem557;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**

557. Reverse Words in a String III

Given a string, you need to reverse the order of characters in each word within a sentence while still preserving whitespace and initial word order.

Example 1:

Input: "Let's take LeetCode contest"
Output: "s'teL ekat edoCteeL tsetnoc"

Note: In the string, each word is separated by single space and there will not be any extra space in the string.

 */

public class Solution {

	public String reverseWords(String s) {
		char[] str = s.toCharArray();
		int a = 0;
		for (int i = 1; i < str.length; i++) {
			if (str[i] == ' ') {
				reverseWord(str, a, i);
				a = i + 1;
			}
		}
		reverseWord(str, a, str.length);
		return new String(str);
	}

	public void reverseWord(char[] str, int a, int b) {
		for (int i = a, j = b - 1; i < j; i++, j--) {
			char tmp = str[i];
			str[i] = str[j];
			str[j] = tmp;
		}
	}

	@Test
	public void testReverseWords() {
		String input =		"Let's take LeetCode contest";
		String expected =	"s'teL ekat edoCteeL tsetnoc";
		Solution sol = new Solution();
		String actual = sol.reverseWords(input);
		assertEquals(expected, actual);
	}

}

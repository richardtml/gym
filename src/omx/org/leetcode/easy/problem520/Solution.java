package omx.org.leetcode.easy.problem520;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**

520. Detect Capital

Given a word, you need to judge whether the usage of capitals in it is right or not.

We define the usage of capitals in a word to be right when one of the following cases holds:

    All letters in this word are capitals, like "USA".
    All letters in this word are not capitals, like "leetcode".
    Only the first letter in this word is capital if it has more than one letter, like "Google".

Otherwise, we define that this word doesn't use capitals in a right way.

Example 1:

Input: "USA"
Output: True

Example 2:

Input: "FlaG"
Output: False

Note: The input will be a non-empty word consisting of uppercase and lowercase latin letters.

 */

public class Solution {

	public boolean detectCapitalUse(String word) {
		int count = 0;
		for (char c : word.toCharArray()) {
			if (Character.isUpperCase(c)) {
				count++;
			}
		}
		return	count == 0 ||
				count == word.length() ||
				(count == 1 && Character.isUpperCase(word.charAt(0)));
	}

	@Test
	public void testDetectCapitalUse1() {
		String word = "USA";
		boolean expected = true;
		Solution sol = new Solution();
		boolean actual = sol.detectCapitalUse(word);
		assertEquals(expected, actual);
	}

	@Test
	public void testDetectCapitalUse2() {
		String word = "Flag";
		boolean expected = true;
		Solution sol = new Solution();
		boolean actual = sol.detectCapitalUse(word);
		assertEquals(expected, actual);
	}

	public void testDetectCapitalUse3() {
		String word = "FlaG";
		boolean expected = false;
		Solution sol = new Solution();
		boolean actual = sol.detectCapitalUse(word);
		assertEquals(expected, actual);
	}

}

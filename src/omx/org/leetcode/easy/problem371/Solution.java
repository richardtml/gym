package omx.org.leetcode.easy.problem371;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**

371. Sum of Two Integers

Calculate the sum of two integers a and b, but you are not allowed to use the operator + and -.

Example:
Given a = 1 and b = 2, return 3.

Credits:
Special thanks to @fujiaozhu for adding this problem and creating all test cases.

 */

public class Solution {

	public int getSum(int a, int b) {
		int i, j, k, sum = 0, carry = 0, mask = 1;
		while (mask != 0) {
			i = a & mask;
			j = b & mask;
			k = i ^ j ^ carry;
			carry = (i & j) | (carry & (i ^ j));
			sum |= k;
			mask <<= 1;
			carry <<= 1;
		}
		return sum;
	}

	@Test
	public void testGetSum() {
		Solution sol = new Solution();
		int n = 10;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				int expected = i + j;
				int actual = sol.getSum(i, j);
				assertEquals(i+"+"+j, expected, actual);
			}
		}
	}

}

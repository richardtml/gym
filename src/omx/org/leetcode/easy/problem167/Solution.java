package omx.org.leetcode.easy.problem167;

import static org.junit.Assert.assertArrayEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

/**

167. Two Sum II - Input array is sorted

Given an array of integers that is already sorted in ascending order, find two numbers such that they add up to a specific target number.

The function twoSum should return indices of the two numbers such that they add up to the target, where index1 must be less than index2. Please note that your returned answers (both index1 and index2) are not zero-based.

You may assume that each input would have exactly one solution and you may not use the same element twice.

Input: numbers={2, 7, 11, 15}, target=9
Output: index1=1, index2=2

 */

public class Solution {

	public int[] twoSum(int[] numbers, int target) {
		Map<Integer,Integer> map = new HashMap<>(numbers.length);
		for (int i = 0; i < numbers.length; i++) {
			int diff = target - numbers[i];
			if (map.containsKey(diff))
				return new int[] {map.get(diff) + 1, i + 1};
			map.put(numbers[i], i);
		}
		return new int[] {0, 0};
	}

	@Test
	public void testTwoSum1() {
		int[] numbers = {2, 7, 11, 15};
		int target = 9;
		int[] expected = {1, 2};
		Solution sol = new Solution();
		int[] actual = sol.twoSum(numbers, target);
		assertArrayEquals(expected, actual);
	}

	@Test
	public void testTwoSum2() {
		int[] numbers = {0, 1, 2, 4};
		int target = 4;
		int[] expected = {1, 4};
		Solution sol = new Solution();
		int[] actual = sol.twoSum(numbers, target);
		assertArrayEquals(expected, actual);
	}

}

package omx.org.leetcode.easy.problem476;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**

476. Number Complement

Given a positive integer, output its complement number. The complement strategy is to flip the bits of its binary representation.

Note:

    The given integer is guaranteed to fit within the range of a 32-bit signed integer.
    You could assume no leading zero bit in the integer’s binary representation.

Example 1:

Input: 5
Output: 2
Explanation: The binary representation of 5 is 101 (no leading zero bits), and its complement is 010. So you need to output 2.

Example 2:

Input: 1
Output: 0
Explanation: The binary representation of 1 is 1 (no leading zero bits), and its complement

 */

public class Solution {

	public int findComplement(int num) {
		int trailing = 31;
		for (int tmp = num; tmp != 0; trailing--)
			tmp = tmp >>> 1;
		int mask = Integer.MAX_VALUE >>> trailing;
		return ~num & mask;
	}

	@Test
	public void testFindComplement1() {
		int input = 5;
		int expected = 2;
		Solution sol = new Solution();
		int actual = sol.findComplement(input);
		assertEquals(expected, actual);
	}

	@Test
	public void testFindComplement2() {
		int input = 1;
		int expected = 0;
		Solution sol = new Solution();
		int actual = sol.findComplement(input);
		assertEquals(expected, actual);
	}

}

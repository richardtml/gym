package omx.org.leetcode.easy.problem136;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**

136. Single Number

Given an array of integers, every element appears twice except for one. Find that single one.

Note:
Your algorithm should have a linear runtime complexity. Could you implement it without using extra memory?

 */


public class Solution {

	public int singleNumber(int nums[]) {
		int result = 0;
		for (int i = 0; i < nums.length; i++)
			result ^= nums[i];
		return result;
	}

	@Test
	public void testSingleNumber() {
		int[] nums = {0, 1, 0, 1, 2};
		int expected = 2;
		Solution sol = new Solution();
		int actual = sol.singleNumber(nums);
		assertEquals(expected, actual);
	}

}

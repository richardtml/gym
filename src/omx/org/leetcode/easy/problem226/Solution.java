package omx.org.leetcode.easy.problem226;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**

226. Invert Binary Tree

Invert a binary tree.

     4
   /   \
  2     7
 / \   / \
1   3 6   9

to

     4
   /   \
  7     2
 / \   / \
9   6 3   1

Trivia:
This problem was inspired by this original tweet by Max Howell:

    Google: 90% of our engineers use the software you wrote (Homebrew), but you can’t invert a binary tree on a whiteboard so fuck off.

 */

public class Solution {

	public TreeNode invertTree(TreeNode root) {
		if (root == null)
			return null;

		TreeNode left = root.left;
		TreeNode right = root.right;
		root.left = invertTree(right);
		root.right = invertTree(left);

		return root;
	}

	public boolean equalsTree(TreeNode a, TreeNode b) {
		if (a == null && b == null)
			return true;
		if (a == null || b == null)
			return false;
		return a.val == b.val && equalsTree(a.left, b.left) && equalsTree(a.right, b.right);
	}


	public static class TreeNode {
		int val;
		public TreeNode left;
		public TreeNode right;
		public TreeNode(int x) { val = x; }
	}

	private TreeNode inputTree() {
		TreeNode d = new TreeNode(1);
		TreeNode e = new TreeNode(3);
		TreeNode f = new TreeNode(6);
		TreeNode g = new TreeNode(9);

		TreeNode b = new TreeNode(2);
		b.left = d;
		b.right = e;

		TreeNode c = new TreeNode(7);
		c.left = f;
		c.right = g;

		TreeNode a = new TreeNode(4);
		a.left = b;
		a.right = c;

		return a;
	}

	private TreeNode expectedTree() {
		TreeNode d = new TreeNode(9);
		TreeNode e = new TreeNode(6);
		TreeNode f = new TreeNode(3);
		TreeNode g = new TreeNode(1);

		TreeNode b = new TreeNode(7);
		b.left = d;
		b.right = e;

		TreeNode c = new TreeNode(2);
		c.left = f;
		c.right = g;

		TreeNode a = new TreeNode(4);
		a.left = b;
		a.right = c;

		return a;
	}

	@Test
	public void testInvertTree() {
		TreeNode root = inputTree();
		TreeNode expected = expectedTree();
		Solution sol = new Solution();
		TreeNode actual = sol.invertTree(root);
		assertTrue(sol.equalsTree(expected, actual));
	}

}

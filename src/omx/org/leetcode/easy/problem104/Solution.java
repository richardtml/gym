package omx.org.leetcode.easy.problem104;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**

104. Maximum Depth of Binary Tree

Given a binary tree, find its maximum depth.

The maximum depth is the number of nodes along the longest path from the root node down to the farthest leaf node.

 */

public class Solution {

    public int maxDepth(TreeNode root) {
    	if (root == null) return 0;
    	return 1 + Math.max(maxDepth(root.left), maxDepth(root.right));
    }

    public static class TreeNode {
    	int val;
    	TreeNode left;
    	TreeNode right;
    	public TreeNode(int x) { val = x; }
    }

	@Test
	public void testMaxDepth() {
		TreeNode root = new TreeNode(0);
		int expected = 1;
		Solution sol = new Solution();
		int actual = sol.maxDepth(root);
		assertEquals(expected, actual);
	}

}

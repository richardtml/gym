package omx.org.leetcode.easy.problem461;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**

461. Hamming Distance

The Hamming distance between two integers is the number of positions at which the corresponding bits are different.

Given two integers x and y, calculate the Hamming distance.

Note:
0 ≤ x, y < 231.

Example:

Input: x = 1, y = 4

Output: 2

Explanation:
1   (0 0 0 1)
4   (0 1 0 0)
       ↑   ↑

The above arrows point to positions where the corresponding bits are different.


 */

public class Solution {

	public int hammingDistance(int x, int y) {
		int diff = x ^ y;
		int bits = 0;
		while (diff != 0) {
			bits += diff & 1;
			diff = diff >>> 1;
		}
		return bits;
    }

	@Test
	public void testHammingDistance1() {
		int inputx = 1;
		int inputy = 4;
		int expected = 2;
		Solution sol = new Solution();
		int actual = sol.hammingDistance(inputx, inputy);
		assertEquals(expected, actual);
	}

	@Test
	public void testHammingDistance2() {
		int x = 1;
		int y = -4;
		int expected = 31;
		Solution sol = new Solution();
		int actual = sol.hammingDistance(x, y);
		assertEquals(expected, actual);
	}

}

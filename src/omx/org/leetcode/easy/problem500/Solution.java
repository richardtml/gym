package omx.org.leetcode.easy.problem500;

import static org.junit.Assert.assertArrayEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

/**

500. Keyboard Row

Given a List of words, return the words that can be typed using letters of alphabet on only one row's of American keyboard like the image below.

American keyboard

Example 1:

Input: ["Hello", "Alaska", "Dad", "Peace"]
Output: ["Alaska", "Dad"]

 */

public class Solution {
	private static Map<Character, Boolean> rowA;
	private static Map<Character, Boolean> rowB;
	private static Map<Character, Boolean> rowC;

	static {
		rowA = new HashMap<>();
		rowA.put('Q', true);
		rowA.put('W', true);
		rowA.put('E', true);
		rowA.put('R', true);
		rowA.put('T', true);
		rowA.put('Y', true);
		rowA.put('U', true);
		rowA.put('I', true);
		rowA.put('O', true);
		rowA.put('P', true);
		rowB = new HashMap<>();
		rowB.put('A', true);
		rowB.put('S', true);
		rowB.put('D', true);
		rowB.put('F', true);
		rowB.put('G', true);
		rowB.put('H', true);
		rowB.put('J', true);
		rowB.put('K', true);
		rowB.put('L', true);
		rowC = new HashMap<>();
		rowC.put('Z', true);
		rowC.put('X', true);
		rowC.put('C', true);
		rowC.put('V', true);
		rowC.put('B', true);
		rowC.put('N', true);
		rowC.put('M', true);
	}

	public String[] findWords(String[] words) {
		List<String> writables = new ArrayList<>();
		for (String word : words) {
			if (	isWritable(word, rowA) ||
					isWritable(word, rowB) ||
					isWritable(word, rowC)) {
				writables.add(word);
			}
		}
		return writables.toArray(new String[writables.size()]);
	}

	private boolean isWritable(String word, Map<Character, Boolean> row) {
		for (char c : word.toCharArray()) {
			if (!row.containsKey(Character.toUpperCase(c)))
				return false;
		}
		return true;
	}

	@Test
	public void testFindWords() {
		String[] input = {"Hello", "Alaska", "Dad", "Peace"};
		String[] expected = {"Alaska", "Dad"};
		Solution sol = new Solution();
		String[] actual = sol.findWords(input);
		assertArrayEquals(expected, actual);
	}

}
